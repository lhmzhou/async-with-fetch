async function loadUsers() {

    return (await fetch("data/users.json")).json(); 
    
    // const response = await fetch("data/users.json");
    // const users = await response.json(); 

    // return users; 
}

document.addEventListener("DOMContentLoaded", async () => {
    let users = []; 

    try {
        const users = await loadUsers(); 
    } catch (e) {
        console.log("Error!"); 
        console.log(e);
    }

    console.log(users);
}); 